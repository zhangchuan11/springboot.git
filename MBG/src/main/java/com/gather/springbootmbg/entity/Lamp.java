package com.gather.springbootmbg.entity;

public class Lamp {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column lamp.id
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column lamp.name
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column lamp.location
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    private String location;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column lamp.id
     *
     * @return the value of lamp.id
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column lamp.id
     *
     * @param id the value for lamp.id
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column lamp.name
     *
     * @return the value of lamp.name
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column lamp.name
     *
     * @param name the value for lamp.name
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column lamp.location
     *
     * @return the value of lamp.location
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    public String getLocation() {
        return location;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column lamp.location
     *
     * @param location the value for lamp.location
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    public void setLocation(String location) {
        this.location = location;
    }
}