package com.gather.springbootmbg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMbgApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMbgApplication.class, args);
    }

}
