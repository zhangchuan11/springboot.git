package com.gather.springbootmbg.mapper;

import com.gather.springbootmbg.entity.LampState;
import java.util.List;

public interface LampStateMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lampstate
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lampstate
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    int insert(LampState record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lampstate
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    LampState selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lampstate
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    List<LampState> selectAll();

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lampstate
     *
     * @mbggenerated Wed May 08 12:52:27 CST 2019
     */
    int updateByPrimaryKey(LampState record);
}